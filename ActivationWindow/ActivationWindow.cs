﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActivationWindow
{
    public partial class ActivationForm : Form
    {
        public ActivationForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void submitButton_click(object sender, EventArgs e)
        {
            submit();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       private void NameBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                submit();
            }
        }

        private void activationBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                submit();
            }
         }

        private void submit()
        {
            NameBox.BackColor = Color.White;
            activationBox.BackColor = Color.White;

            if (NameBox.Text.Length != 0 && activationBox.Text.Length != 0)
            {
                statusLabel.Text = "Name: " + NameBox.Text + "\nActivation Code: " + activationBox.Text;
            }
            else if (NameBox.Text.Length == 0)
            {
                statusLabel.Text = "Please Enter A Name.";
                NameBox.BackColor = Color.Yellow;
                NameBox.Focus();
            }
            else if (activationBox.Text.Length == 0)
            {
                statusLabel.Text = "Please Enter Your Activation Code.";
                activationBox.BackColor = Color.Yellow;
                activationBox.Focus();
            }
        }
    }
}
